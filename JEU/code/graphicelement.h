#ifndef GRAPHICELEMENT_H
#define GRAPHICELEMENT_H
#include <SFML/Graphics.hpp>

class GraphicElement:public sf::Sprite
{
private:
    int _w,_h;
public:
    GraphicElement(sf::Texture &image, int w, int h);
    int getWidth();
    void draw(sf::RenderWindow * window);
    void resize(sf::Vector2f dimension);
    void updatePos(sf::Vector2f position);
    virtual void animate();


};

#endif // GRAPHICELEMENT_H
