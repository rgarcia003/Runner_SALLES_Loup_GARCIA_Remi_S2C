#include "highscores.h"



HighScores::HighScores()
{
    _font = new sf::Font();
    if(!_font->loadFromFile(FONT2))
    {
        std::cerr << "ERROR when loading font file: "
                  << FONT << std::endl;
        exit(EXIT_FAILURE);
    }
    _text.setFont(*_font);
    _text.setPosition(50, 20);
    _text.setCharacterSize(20);

}

HighScores::~HighScores()
{
    delete _font;
}

void HighScores::dispScores(sf::RenderWindow *window)// affiche un ecran de meilleurs scores
{
    int i = 0;
    _fscores.open(SCORES.c_str(), std::ios::in);
    std::string sc;
    std::string tmpsc = "";
    if(_fscores.fail())
    {
        std::cerr << "ERROR when loading text file: "
                  << SCORES << std::endl;
        exit(EXIT_FAILURE);
    }
    else
    {

        tmpsc = "Best Scores!! \n";
        for (i = 0; i < 10; i++)
        {

            std::getline(_fscores, sc);
            tmpsc += sc + "\n";

        }
        _fscores.close();
        _text.setString(tmpsc);
        _text.setColor(sf::Color::White);
        window->clear();
        window->draw(_text);
        window->display();
    }
}

void HighScores::addScores(unsigned int score)//ajoute un score dans le fichier txt et trie le fichier
{
    std::fstream ftmpScores;
    bool end = false;
    unsigned int tscore;
    int i = 0;

    ftmpScores.open("tmp.txt", std::ios::out);
    _fscores.open(SCORES.c_str(), std::ios::in);
    if (ftmpScores.fail())
    {
        std::cerr << "ERROR when loading text file: "
                  << "ftmpScores" << std::endl;

        exit(EXIT_FAILURE);
    }
    else if(_fscores.fail())
    {
        std::cerr << "ERROR when loading text file: "
                  << SCORES << std::endl;
        exit(EXIT_FAILURE);
    }
    else
    {
        while (!_fscores.eof() && !end)
        {
            _fscores >> tscore;
            if (score > tscore)
                end = true;
            else
                ftmpScores << tscore << std::endl;
        }

        ftmpScores << score << std::endl;

        while (!_fscores.eof())
        {
            ftmpScores << tscore << std::endl;
            _fscores >> tscore;
        }

        ftmpScores.close();
        _fscores.close();
    }
    ftmpScores.open("tmp.txt", std::ios::in);
    _fscores.open(SCORES.c_str(), std::ios::out);
    if (ftmpScores.fail())
    {
        std::cerr << "ERROR when loading text file: "
                  << "ftmpScores" << std::endl;
        exit(EXIT_FAILURE);
    }
    else if(_fscores.fail())
    {
        std::cerr << "ERROR when loading text file: "
                  << SCORES << std::endl;
        exit(EXIT_FAILURE);
    }
    else
    {
        for(i = 0; i < 10; i++)
        {
            ftmpScores >> tscore ;
            _fscores << tscore << std::endl;
        }

        ftmpScores.close();
        _fscores.close();
        if(remove("tmp.txt")!=0)
        {
            std::cerr << "impossible de supprimer tmp.txt" << std::endl;

        }

    }


}

