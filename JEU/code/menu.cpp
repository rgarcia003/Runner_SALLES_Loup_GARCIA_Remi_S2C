#include "menu.h"

Menu::Menu()
    :_window{true}
{}

void Menu::afficherMenu(sf::Vector2i pos_Souris, std::vector<std::pair<clickableselement,sf::Text>>& _buttons, bool &in_Menu)
{


    for(int i=0;i<_buttons.size();i++)

    {
        if( _buttons[i].first.GetClickable(pos_Souris))
            _buttons[i].second.setColor(sf::Color::Red);
        else
            _buttons[i].second.setColor(sf::Color::White);

    }

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) )
    {

        if(_buttons[0].first.GetClickable(pos_Souris))
        {
            in_Menu = false;

        }
        else if (_buttons[1].first.GetClickable(pos_Souris))
        {

            _window = false;
        }
    }

}

bool Menu::getWindow() const
{
    return _window;
}

