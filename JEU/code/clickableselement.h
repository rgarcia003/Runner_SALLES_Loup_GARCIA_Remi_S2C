#ifndef CLICKABLESELEMENT_H
#define CLICKABLESELEMENT_H
#include <SFML/Graphics.hpp>



class clickableselement
{
private:
    int _x;
    int _y;
    int _w;
    int _h;
    bool _isclicked;

public:
    clickableselement(int x, int y, int w, int h);
    bool GetClickable(sf::Vector2i pos_Souris);
    int X();
    int Y();
    int W();
    int H();
};

#endif // CLICKABLESELEMENT_H
