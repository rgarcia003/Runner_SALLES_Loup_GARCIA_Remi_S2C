#include "View.h"
#include "Model.h"
#include<iostream>
#include <fstream>

#include <sstream>
#include <iostream>
using namespace std;
//using namespace sf;


//=======================================
// Constructeur
//=======================================
View::View(int w, int h)
    : _w(w),_h(h),_inmenu{true}, _inintro{true}// w est affecte à _w et h à _h
{

    _window = new sf::RenderWindow(sf::VideoMode(w, h, 32), "Runner", sf::Style::Close);//le RenderWindow est un constructeur de la fenetre
    // equivalent du SDL_SetVideoMode(....) la fenetre fait donc w de largeur, h de hauteur, avec 32 bits par pixel.
    //Elle s'appelle "Runner". De plus, la fenetre possede une bouton de fermeture
    _window->setFramerateLimit(40);//on fait un tour de boucle tous les 40emes de seconde

    if (!_background1.loadFromFile(BACKGROUND_IMAGE1)) {//si l'image du background "BACKGROUND_IMAGE" n'est pas trouvee dans le repertoire de travail
        std::cerr << "ERROR when loading image file: "//le programme avertit l'utilisateur qu'il n'a pas pu charger... l'image en question
                  << BACKGROUND_IMAGE1 << std::endl;
        exit(EXIT_FAILURE);//inverse du EXIT_SUCCESS, on quitte le programme en renvoyant un code d'erreur (comme un return 1 ou 2)

    }
    if (!_background2.loadFromFile(BACKGROUND_IMAGE2)) {
        std::cerr << "ERROR when loading image file: "
                  << BACKGROUND_IMAGE2 << std::endl;
        exit(EXIT_FAILURE);
    }

    if (!_ball.loadFromFile(BALL_IMAGE)) {//idem pour la balle
        std::cerr << "ERROR when loading image file: "
                  << BALL_IMAGE << std::endl;
        exit(EXIT_FAILURE);
    }

    if (!_rock.loadFromFile(BALL_IMAGE_ROCK)) {//idem pour la balle
        std::cerr << "ERROR when loading image file: "
                  << BALL_IMAGE_ROCK << std::endl;
        exit(EXIT_FAILURE);
    }
    if (!_obstacle.loadFromFile(OBSTACLE_IMAGE)) {
        std::cerr << "ERROR when loading image file: "
                  << OBSTACLE_IMAGE << std::endl;
        exit(EXIT_FAILURE);
    }
    if (!_obstacle2.loadFromFile(OBSTACLE2_IMAGE)) {
        std::cerr << "ERROR when loading image file: "
                  << OBSTACLE2_IMAGE << std::endl;
        exit(EXIT_FAILURE);
    }


    _font = new sf::Font();
    if(!_font->loadFromFile(FONT))
    {
        std::cerr << "ERROR when loading image file: "
                  << FONT << std::endl;
        exit(EXIT_FAILURE);
    }

    _font2 = new sf::Font();
    if(!_font2->loadFromFile(FONT2))
    {
        std::cerr << "ERROR when loading image file: "
                  << FONT2 << std::endl;
        exit(EXIT_FAILURE);
    }

    if(!_bonus.loadFromFile(BONUS_IMAGE))
    {
        std::cerr << "ERROR when loading image file: "
                  << BONUS_IMAGE << std::endl;
        exit(EXIT_FAILURE);
    }

    if(!_life.loadFromFile(LIFEBAR_IMAGE))
    {
        std::cerr << "ERROR when loading image file: "
                  << LIFEBAR_IMAGE << std::endl;
        exit(EXIT_FAILURE);
    }
    if(!Intro.loadFromFile(INTRO))
    {
        std::cerr << "ERROR when loading image file: "
                  << INTRO << std::endl;
        exit(EXIT_FAILURE);
    }



    else {



        _lifeSprite= new GraphicElement(_life,150,30);
        _lifeSprite->resize({156,30});
        _lifeSprite->setPosition(w-200,h-50);
        lifeAmmount.setFillColor(sf::Color::Cyan);
        lifeAmmount.setPosition(w-197,h-47);
        _backgroundSprite1= new SlidingBackground(_background1,_w,_h,3);// les arrieres plan par paralaxe
        _backgroundSprite2= new SlidingBackground(_background2,_w,_h,2);



    }

    // permet de positionner le texte dans la fenêtre

    text.setCharacterSize(30);
    text.setFont(*_font);
    text.setColor(sf::Color::Black);


}



//=======================================
// Destructeur
//=======================================
View::~View(){

    if(_window!= NULL){// si la fenetre existe
        delete _window;//on supprime la fenetre: equivalent du SDL_FreeSurface(screen);

        delete introduction;
        delete _backgroundSprite1;
        delete _backgroundSprite2;
        delete _font;
        delete _font2;
        delete _lifeSprite;
        for (auto it: _elementToGraphicElement)
            delete it.second;
    }

}

//=======================================
// Accesseurs en écriture
//=======================================
void View::setModel(Model * model){
    _model = model;//on associe la vue au modele

    text.setFont(*_font);
    text.setColor(sf::Color::Black);
    text.setPosition(_w-500,_h-50);

    _model->Reset();

}

//=======================================
// Fonctions de dessin
//=======================================
void View::draw(){
    _window->clear();//le contenu eventuel de la fenetre est efface.


    _backgroundSprite2->draw(_window);
    _backgroundSprite1->draw(_window);// equivalent du SDL_BlitSurface (ou applySurface de Mr Journet): les donnees sont chargees dans la carte graphique depuis la RAM
    _lifeSprite->draw(_window);
    _window->draw(lifeAmmount);
    _window->draw(text);


    MovableElement * ball=_model->getMovableElements().front();
    if(!ball->getVulnerable())
        _elementToGraphicElement[ball]->setTexture(_rock);//changement de sprite de la balle si elle est invincible
    else if(_elementToGraphicElement[ball]->getTexture() != &_ball)
        _elementToGraphicElement[ball]->setTexture(_ball);

    for (auto it : _elementToGraphicElement)

    {
        _window->draw(*it.second);
    }

    if(ball->getLife()<=0)//ecran de game over
    {
        text.setFont(*_font2);
        text.setCharacterSize(60);
        text.setColor(sf::Color::White);
        text.setPosition({0.33*_w,0.33*_h});
        text.setString("GAME OVER");
        _window->draw(text);
        _window->display();

        HighScores h;
        h.addScores(_model->getScore());// le score courant est ajoute a scores.txt

        sf::sleep((sf::seconds(4)));

        h.dispScores(_window);//ecran de best scores

        sf::sleep((sf::seconds(4)));

        Reset();
        _inmenu=true;

    }

    _window->display();//equivalent du SDL_Flip(screen); toutes les donnees vont de la carte graphique a l ecran
}
//=======================================
// Mise a jour des objets graphiques
//=======================================
void View::synchronize(){

    _backgroundSprite1->setSpeed(-1*_model->getSpeed()/2);
    _backgroundSprite2->setSpeed(-1*_model->getSpeed()/3);

    MovableElement * ball=_model->getMovableElements().front();

    MovableElement *last =_model->getMovableElements().back();

    if(_elementToGraphicElement.find(last)
            ==_elementToGraphicElement.end())// pour eviter que le meme traitement soit applique plusieurs fois au meme objet
    {

        if(_elementToGraphicElement.find(ball)
                ==_elementToGraphicElement.end())
        {
            _elementToGraphicElement[ball]=new GraphicElement(_ball,50,50);
            _elementToGraphicElement[ball]->resize(_model->getBallDimension());

        }

        if(dynamic_cast<coins*>(last)){
            vector<sf::IntRect> lectureRects;
            for(int i=0;i<5;i++)
            {
                lectureRects.push_back({50*i,0,50,50});//la piece n'a qu'un seul type et ajuste ses rectangles de lecture en horizontal uniquement
            }

            _elementToGraphicElement[last]=new AnimatedGraphicElement(lectureRects,_bonus,50,50);
        }
        else if (dynamic_cast<Bonus*>(last))
        {

            vector<sf::IntRect> lectureRects;
            for(int i=0;i<5;i++)
            {
                lectureRects.push_back({50*i,(last->getType()+1)*50 ,50,50});//selon le type du bonus(en int) les rectangles de lecture suivent la feuille de sprites pour une dimension de 50x50
            }

            _elementToGraphicElement[last]=new AnimatedGraphicElement(lectureRects,_bonus,50,50);
        }

        else if (dynamic_cast<Obstacle*>(last))
        { switch (last->getType()) {//nous avons ici les trois types d'obstacles
            case 0:
                _elementToGraphicElement[last]=new GraphicElement(_obstacle,80,150);
                break;
            case 1:
                _elementToGraphicElement[last]=new GraphicElement(_obstacle2,110,110);
                break;
            case 2:
                _elementToGraphicElement[last]=new GraphicElement(_obstacle2,110,55);
            default:
                break;
            }

        }

        _elementToGraphicElement[last]->resize(_model->getMovableElements().back()->getElemDimension());// chaque nouvel arrivant est resize
    }


    for (const auto &it : _elementToGraphicElement)

    {
        auto movables=_model->getMovableElements();
        if(find(movables.begin(),movables.end(),it.first) != movables.end()){
            it.second->updatePos(it.first->getElemPosition());// chaque graphique adopte la position de son movable
            if(dynamic_cast<AnimatedGraphicElement*>(it.second))
                it.second->animate();// les bonus tournent

        }
        else{
            _graphicsToKill.insert(it);
        }
    }
    for(const auto &it: _graphicsToKill){//comme dans modele , on adopte le principe de la corbeille
        delete it.second;
        _elementToGraphicElement.erase(it.first);

    }
    _graphicsToKill.clear();


    lifeAmmount.setSize({1.0*_lifeSprite->getWidth()*(1-(1.0*(ball->getLifeMax()-ball->getLife())/ball->getLifeMax())),24});
    //la barre de vie sera de la taille du sprite du conteneur de vie si la balle a atteint sa vie max, quelle que soit le montant du max


    text.setString("Score: "+to_string(_model->getScore()));



}


//=======================================
// Traitement des evenements
//=======================================
bool View::treatEvents(){
    bool result = false;//on  part du principe qu'il n'y a pas d'evenements a traiter
    if(_window->isOpen()){//si la fenetre est ouverte
        result = true;// alors on peut traiter des evenements

        sf::Event event;//on declare un event pour traitemenr futur
        while (_window->pollEvent(event)) {// equivalent du SDL_PollEvent(event) qui s'active des qu'un evenement est detecte. C'est le "conteneur" des evenements
            cout << "Event detected" << endl;//et on en avertit l'utilisateur

            if ((event.type == sf::Event::Closed) ||
                    ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))) {//si l'utilisateur appuie sur la croix en haut a droite ou sur echap, alors
                _window->close();// equivalent de SDL_Quit(); on ferme la fenetre
                result = false;//et il n'y aura plus d'evenements a traiter
            }

            if (event.type == sf::Event::KeyPressed)
            {
                switch (event.key.code) {
                case sf::Keyboard::Up:
                    _model->jump();
                    break;
                case sf::Keyboard::Left:
                    _model->moveBall(true);

                    break;
                case sf::Keyboard::Right:
                    _model->moveBall(false);
                    break;


                default:
                    break;
                }

            }

            else if(event.type==sf::Event::KeyReleased)
            {
                if(event.key.code==sf::Keyboard::Left || event.key.code==sf::Keyboard::Right)

                    _model->stopBallX();

            }
        }
    }
    return result;
}

void View::drawMenu()
{

    _window->clear();

    if(_inintro){// gere l'ecran d'introduction du jeu
        introduction= new GraphicElement(Intro,_w,_h);
        introduction->draw(_window);
        _window->display();
        sf::sleep(sf::seconds(2));
        _inintro=false;
    }

    sf::Event event;//on declare un event pour traitement futur


    sf::Text tplay;
    sf::Text tquit;
    sf::Text tdiff;
    sf::Text tlevel;
    sf::Text title;

    title.setCharacterSize(80);

    title.setFont(*_font2);
    tdiff.setFont(*_font2);
    tplay.setFont(*_font2);
    tquit.setFont(*_font2);
    tlevel.setFont(*_font2);

    tdiff.setPosition(465, 100);
    tplay.setPosition(300, 275);
    tquit.setPosition(850, 275);
    tlevel.setPosition(765, 100);

    title.setPosition(415, 450);

    title.setString("Runner");
    tdiff.setString("Difficulty : ");
    tplay.setString("Play");
    tquit.setString("Quit");
    tlevel.setString(" Easy");
    globals::EASYNESS=100;



    clickableselement play(300, 275, 100, 50);
    clickableselement quit(850, 275, 100, 50);
    clickableselement diff(465, 100, 300, 50);

    std::vector<pair<clickableselement,sf::Text>> _buttons;
    _buttons.push_back( std::make_pair(play,tplay));
    _buttons.push_back(std::make_pair(quit,tquit));

    bool trigger=false;
    Menu menu;
    sf::Vector2i posSouris;
    while (_inmenu && _window->isOpen())
    {
        _backgroundSprite2->draw(_window);
        _backgroundSprite1->draw(_window);

        _window->draw(title);
        _window->draw(tdiff);
        _window->draw(tlevel);
        for(auto it: _buttons)
            _window->draw(it.second);

        _window->display();

        posSouris = sf::Mouse::getPosition(*_window);
        menu.afficherMenu(posSouris,_buttons, _inmenu);//gere les boutons qui font sortir du menu
        while (_window->pollEvent(event)) {// equivalent du SDL_PollEvent(event) qui s'active des qu'un evenement est detecte. C'est le "conteneur" des evenements

            if (diff.GetClickable(posSouris))//si le curseur est sur le texte de difficulte
            {

                tdiff.setColor(sf::Color::Red);//il passe au rouge

                if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left)

                    trigger=true;

            }
            else
                tdiff.setColor(sf::Color::White);

            if (diff.GetClickable(posSouris)&& trigger &&event.type == sf::Event::MouseButtonReleased)
            {

                if (tlevel.getString() == " Easy"){// et en cas de click la difficulte est modifiee

                    tlevel.setString(" Hard");

                    globals::EASYNESS=40;
                }
                else{
                    tlevel.setString(" Easy");
                    globals::EASYNESS=100;
                }
                trigger=false;

            }


            cout << "Event detected" << endl;//et on en avertit l'utilisateur

            if ((event.type == sf::Event::Closed) ||
                    ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))) {//si l'utilisateur appuie sur la croix en haut a droite ou sur echap, alors
                _inmenu=false;
                _window->close();// equivalent de SDL_Quit(); on ferme la fenetre


            }
        }

        if (!menu.getWindow()){
            _inmenu=false;
            _window->close();

        }


    }




}

bool View::GetIfMenu() const
{
    return _inmenu;
}

void View::setMenu(bool menu)

{
    _inmenu=menu;
}

void View::Reset()
{
    text.setFont(*_font);
    text.setColor(sf::Color::Black);
    text.setCharacterSize(30);
    text.setPosition(_w-500,_h-50);
    _backgroundSprite1->setSpeed(3);
    _backgroundSprite2->setSpeed(2);

    _inmenu=true;
}

