#include "graphicelement.h"


GraphicElement::GraphicElement(sf::Texture & image, int w, int h)
    :_w{w},_h{h}
{

    this->setTexture(image);


}

void GraphicElement::draw(sf::RenderWindow * window)

{
    window->draw(*this);
}

void GraphicElement::resize(sf::Vector2f dimension)
{

    sf::FloatRect bb = this->getLocalBounds();      // retourne les positions et taille réelles de s
    float width_factor = 1.0*dimension.x / bb.width;     // facteur de mise à l'échelle pour la largeur
    float height_factor = 1.0*dimension.y / bb.height;  // facteur de mise à l'échelle pour la largeur
    this->setScale(width_factor, height_factor);
}
int GraphicElement::getWidth()
{
    return _w;
}


void GraphicElement::updatePos(sf::Vector2f position)
{
    this->setPosition(position.x,position.y);

}


void GraphicElement::animate()//sera utilise par les AnimatedGraphicElement uniquement
{

}
