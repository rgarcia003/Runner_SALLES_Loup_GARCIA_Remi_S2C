TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += Ball.cc graphicelement.cpp main.cpp Model.cpp movableelement.cpp slidingbackground.cpp View.cpp \
    animatedgraphicelement.cpp \
    bonus.cpp \
    obstacle.cpp \
    coins.cpp \
    Timer.cpp \
    clickableselement.cpp \
    menu.cpp \
    highscores.cpp

HEADERS += Ball.h graphicelement.h Model.h movableelement.h slidingbackground.h View.h \
    animatedgraphicelement.h \
    bonus.h \
    obstacle.h \
    coins.h \
    Timer.h \
    clickableselement.h \
    menu.h \
    globals.h \
    highscores.h

QMAKE_CXXFLAGS += -std=c++11 -Wall -Wextra
LIBS           += -lsfml-graphics -lsfml-window -lsfml-system 

# décommenter pour les tests BOOST
#LIBS          +=  -lboost_unit_test_framework
