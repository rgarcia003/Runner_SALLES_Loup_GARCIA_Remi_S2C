#ifndef OBSTACLE_H
#define OBSTACLE_H

#include"movableelement.h"
class Obstacle:public MovableElement
{
private:
    int _typeObstacle;

public:
    Obstacle(int x, int y, int w, int h, int dx, int dy,int type);
    int getType() const override;

};

#endif // OBSTACLE_H
