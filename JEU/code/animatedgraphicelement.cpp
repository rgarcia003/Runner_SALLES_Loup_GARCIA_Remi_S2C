#include "animatedgraphicelement.h"


AnimatedGraphicElement::AnimatedGraphicElement(const std::vector<sf::IntRect> & clipRects, sf::Texture &image, int w, int h)
    :GraphicElement(image,w,h),_clip_rects{clipRects},time_gap{0},_nb_steps{clipRects.size()},_current_step{0}
{
    this->setTextureRect(clipRects.front());
    this->setTexture(image);
}


void AnimatedGraphicElement::animate()
{
    if(++time_gap%7==0)// vitesse arbitraire a modifier pour la rotation
        this->setTextureRect(_clip_rects[(_current_step++)%_nb_steps]);


}
