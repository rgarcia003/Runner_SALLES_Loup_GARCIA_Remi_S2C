#ifndef ANIMATEDGRAPHICELEMENT_H
#define ANIMATEDGRAPHICELEMENT_H
#include"graphicelement.h"

class AnimatedGraphicElement: public GraphicElement
{
private:
    std::vector<sf::IntRect> _clip_rects; //vecteur de rectangles de lecture à utiliser pour animer l'objet graphique
    int time_gap;
    int _nb_steps;
    int _current_step;
public:
    AnimatedGraphicElement(const std::vector<sf::IntRect> & clipRects, sf::Texture &image, int w, int h) ;
    void draw(  sf::RenderWindow * window);
    void animate() override;

};

#endif // ANIMATEDGRAPHICELEMENT_H
