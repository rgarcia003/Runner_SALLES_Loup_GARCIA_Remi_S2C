#ifndef MENU_H
#define MENU_H
#include "View.h"
#include<vector>
#include"Model.h"
class Menu
{
private:

    bool _window;
public:
    Menu();
    void afficherMenu(sf::Vector2i pos_Souris, std::vector<std::pair<clickableselement, sf::Text> > &_buttons, bool &in_Menu);
    clickableselement getPlay() const ;
    clickableselement getQuit() const ;
    bool getWindow() const ;

};

#endif // MENU_H
