#ifndef _MODEL_
#define _MODEL_
#include"Ball.h"
#include"obstacle.h"
#include"bonus.h"
#include"coins.h"
#include"Timer.h"
#include <SFML/Graphics.hpp>
class Ball;


class Model {
protected:
    int _w, _h;
    int _score;
    Timer m_timer;
    Ball *b;
    std::vector<MovableElement*> _elements;
    std::vector<MovableElement*> _elementsToKill;
    float speed;


public:

    Model(int w, int h);
    ~Model();


    void nextStep();
    sf::Vector2f getBallDimension() const;
    sf::Vector2f getBallPosition() const;

    void moveBall(bool left);
    void jump();
    void evolutionJump();
    void stopBallX();
    void addScore(int score);
    void addBonus(int type);
    void addObstacle();
    void addCoin();
    int getScore();
    void accelerate();
    float getSpeed() const;
    std::vector<MovableElement *>getMovableElements() const;
    void Reset();


};
#endif
