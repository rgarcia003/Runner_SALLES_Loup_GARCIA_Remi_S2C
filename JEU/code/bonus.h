#ifndef BONUS_H
#define BONUS_H
#include"movableelement.h"
#include "Model.h"

class Bonus:public MovableElement
{
private:
   int _typeBonus;
public:
   Bonus(int x, int y, int w, int h, int dx, int dy, int typeBonus);
   int getType() const override;

};

#endif // BONUS_H
