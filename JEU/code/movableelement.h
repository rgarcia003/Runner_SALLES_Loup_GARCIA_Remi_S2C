#ifndef MOVABLEELEMENT_H
#define MOVABLEELEMENT_H
#include <SFML/Graphics.hpp>

class MovableElement
{
protected:
    float _x,_y,_w,_h,_dx, _dy;
public:

    virtual void move();// déplace le MovableElement en fonction de _dx et _dy


    int getElemX() const;
    int getElemY() const;
    void setElemSpeedX(int dx);
    void setElemSpeedY(int dy);
    sf::Vector2f getElemPosition() const;
    sf::Vector2f getElemDimension() const;
    int getElemSpeedX() const;
    int getElemSpeedY() const;

    int getElemWidth() const;
    int getElemHeight() const;

    virtual int getType() const;
    virtual int getLife() const;
    virtual int getLifeMax() const;
    virtual bool getVulnerable() const;
    virtual void addLife(int life);

    MovableElement(float x, float y, float w, float h, float dx, float dy);
    virtual ~MovableElement(){}
};

#endif // MOVABLEELEMENT_H
