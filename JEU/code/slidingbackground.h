#ifndef SLIDINGBACKGROUND_H
#define SLIDINGBACKGROUND_H
#include"graphicelement.h"



class SlidingBackground
{

private:
    GraphicElement *_left, *_right;
    int _width, _height;
    unsigned int _speed;
public:

    SlidingBackground(sf::Texture & image, int w, int h, unsigned int speed);
    void draw(sf::RenderWindow *window);
    void setSpeed(unsigned int speed);

    ~SlidingBackground();
};

#endif // SLIDINGBACKGROUND_H
