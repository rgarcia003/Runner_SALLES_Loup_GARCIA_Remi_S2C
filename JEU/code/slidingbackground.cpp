#include "slidingbackground.h"
#include"graphicelement.h"

SlidingBackground::SlidingBackground(sf::Texture & image, int w, int h, unsigned int speed)
    :_width{w},_height{h},_speed{speed}
{
    _left = new GraphicElement(image, w, h);
    _right = new GraphicElement(image, w, h);
    _left->setPosition(sf::Vector2f(0,0));// l'image a pour points d'ancrage les dimensions de la fenetre
    _right->setPosition(sf::Vector2f(w,0));


}
void SlidingBackground::draw(sf::RenderWindow * window)
{

    _left->setPosition(sf::Vector2f(_left->getPosition().x-_speed,_left->getPosition().y));
    _right->setPosition(sf::Vector2f(_right->getPosition().x-_speed,_right->getPosition().y));
    if(_left->getPosition().x+_width<0)//si l'image a fait un "tour" complet, elle revient a ses coordonnes de depart
    {
        _left->setPosition(sf::Vector2f(0,0));
        _right->setPosition(sf::Vector2f(this->_width,0));

    }
    window->draw(*_left);
    window->draw(*_right);
}

void SlidingBackground::setSpeed(unsigned int speed)
{
    _speed=speed;
}

SlidingBackground::~SlidingBackground()
{
    delete _left;
    delete _right;
}
