#include "Model.h"
#include"Timer.h"
#include"globals.h"
#include <iostream>
#include <fstream>
using namespace std;

//=======================================
// Constructeurs
//=======================================
Model::Model(int w, int h)
    :  _w(w), _h(h),_score{0},speed{-6} {

    b= new Ball(100,globals::BALL_HEIGHT,50,50,0,0);
    _elements.push_back(b);
    m_timer.Reset();


}

//=======================================
// Destructeurs
//=======================================
Model::~Model(){


    for(auto it: _elements)
        delete it;


}

//=======================================
// Calcul la prochaine étape
//=======================================
void Model::nextStep(){

    if(m_timer.GetTime()>5)
    {
        addScore(20);
        m_timer.Reset();
        accelerate();
    }

    if(random()%globals::EASYNESS==1 &&
            _elements.back()->getElemX()+_elements.back()->getElemWidth()<_w-(b->getElemWidth()*2))// apparition aleatoire des obstacles
        addObstacle();

    if(random()%globals::EASYNESS==1 &&
            _elements.back()->getElemX()+_elements.back()->getElemWidth()<_w-_elements.back()->getElemWidth())// apparition aleatoire des pieces
        addCoin();

    if(random()%(200-globals::EASYNESS)==1 &&
            _elements.back()->getElemX()+_elements.back()->getElemWidth()<_w-_elements.back()->getElemWidth())// apparition aleatoire des bonus
    {
        int type=random()%50;
        if (type<25)
            addBonus(0);
        else if (type<35)
            addBonus(1);
        else if (type<45)
            addBonus(2);
        else if (type<50)
            addBonus(3);
    }
    for(auto it: _elements){
        if (it->getElemX() + it->getElemWidth()< 0)
        {
            _elementsToKill.push_back(it);
        }
        else

            it->move();
    }


    if(_elements.size()>1)
        for(const auto &i: _elements)
            if(i!=b){
                sf::IntRect ball (b->getElemX(),b->getElemY(),b->getElemWidth(),b->getElemHeight());
                sf::IntRect elem (i->getElemX(), i->getElemY(), i->getElemWidth(), i-> getElemHeight());

                if(ball.intersects(elem))
                {
                    if(dynamic_cast<coins*>(i))
                        addScore(100);
                    else if (dynamic_cast<Bonus*>(i))
                        b->activateBonus(i->getType());
                    else if (dynamic_cast<Obstacle*>(i)&& b->getVulnerable())
                        b->addLife(-20);



                    _elementsToKill.push_back(i);

                }
            }

    for(const auto& it : _elementsToKill){//corbeille contenant les elements a supprimer
        delete it;
        _elements.erase(find(_elements.begin(),_elements.end(),it));
    }

    _elementsToKill.clear();
    evolutionJump();
    b->verifyBonus();

}



sf::Vector2f Model::getBallDimension() const
{
    return sf::Vector2f(b->getElemWidth(),b->getElemHeight());
}

sf::Vector2f Model::getBallPosition() const
{
    return sf::Vector2f(b->getElemX(),b->getElemY());
}



void Model::moveBall(bool left)//établit la vitesse horizontale
{
    if(left)
        b->setElemSpeedX(speed/2);

    else
        b->setElemSpeedX(-1.0*speed);

}

void Model::jump()//donne l'impulsion du saut
{
    if(!b->Isjumping() || b->IsFlying())
    {
        b->setJump(true);
        b->setElemSpeedY(-20);


    }
}

void Model::evolutionJump()// imite le principe d'attraction constante vers le sol
{
    if(b->Isjumping()||b->IsFlying())
    {
        b->setElemSpeedY(b->getElemSpeedY()+1);

        if(b->getElemY()>=globals::BALL_HEIGHT){

            b->setJump(false);
        }

    }


}


void Model::stopBallX()
{
    b->setElemSpeedX(0);
}

std::vector<MovableElement *> Model::getMovableElements() const
{
    return _elements;

}

void Model::addObstacle()
{    int type=random()%3;
     switch (type)
     {
     case 0:
         _elements.push_back(new Obstacle(_w,_h-200,80,150,speed,0,type));
         break;
     case 1:
         _elements.push_back(new Obstacle(_w,_h-160,110,110,speed,0,type));
         break;
     case 2:
         _elements.push_back(new Obstacle(_w,_h-105,110,55,speed,0,type));
         break;

     }
}

void Model::addBonus(int type)
{
    _elements.push_back(new Bonus(_w,globals::BALL_HEIGHT-random()%150,30,30,speed,0,type));
}
void Model::addCoin()
{
    _elements.push_back(new coins(_w,globals::BALL_HEIGHT-random()%150,30,30,speed,0));
}

void Model::addScore(int score)
{
    _score+=score;
}
int Model::getScore()
{
    return _score;
}
void Model::accelerate()
{
    if(speed<30)
        speed*=1.05;

}

float Model::getSpeed() const
{
    return speed;
}

void Model::Reset()
{
    _score=0;

    for(auto it : _elements)
        delete it;


    _elements.clear();

    b= new Ball(100,globals::BALL_HEIGHT,50,50,0,0);
    _elements.push_back(b);
    m_timer.Reset();
    speed=-6;


}
