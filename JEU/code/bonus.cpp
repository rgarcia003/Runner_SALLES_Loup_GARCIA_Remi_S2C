#include "bonus.h"

Bonus::Bonus(int x, int y, int w, int h, int dx, int dy, int typeBonus)
    :MovableElement(x,y,w,h,dx,dy), _typeBonus{typeBonus}
{}


int Bonus::getType() const
{
    return _typeBonus;
}

