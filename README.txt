Ce projet a été réalisé par Loup SALLES et Rémi GARCIA en mai 2016. Il est sous licence GPLv2.

Pour profiter de notre superbe jeu, rendez-vous dans le répertoire JEU/code/ et ouvrez le fichier code.pro avec QtCreator. Il vous demandera alors de configurer le projet, faites le.

Vérifiez bien que le répertoire de compilation correspond à Runner_SALLES_Loup_GARCIA_Remi_S2C/JEU/build-code-Desktop-Debug . 
La compilation se fera de manière classique depuis l'interface de Qt, avec un Ctrl+B

Pour exécuter le projet, le principe est proche, il vous faut faire Ctrl+R... And Enjoy!
