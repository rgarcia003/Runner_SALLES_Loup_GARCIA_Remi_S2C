#ifndef _VIEW_
#define _VIEW_

#include <SFML/Graphics.hpp>
#include<graphicelement.h>
#include<animatedgraphicelement.h>
#include<movableelement.h>
#include<slidingbackground.h>
#include<clickableselement.h>
#include<menu.h>
#include"globals.h"
#include<highscores.h>

const std::string FONT2 = "../Polices/Brown Fox.ttf";


const std::string BACKGROUND_IMAGE1 = "../Images/city_1.png";
const std::string BACKGROUND_IMAGE2 = "../Images/city_2.png";

const std::string BALL_IMAGE = "../Images/ball.png";
const std::string BALL_IMAGE_ROCK = "../Images/ball2.png";
const std::string FONT = "../Polices/Antique Olive.ttf";
const std::string OBSTACLE_IMAGE = "../Images/ennemies.png";
const std::string OBSTACLE2_IMAGE = "../Images/ennemy_block.png";
const std::string BONUS_IMAGE = "../Images/bonus.png";
const std::string LIFEBAR_IMAGE ="../Images/life.png";
const std::string BUTTON = "../Images/buttons.png";
const std::string INTRO = "../Images/Intro.png";



class Model;

class View {
private:
    int _w, _h;

    sf::RenderWindow * _window;
    Model * _model;
    std::map<MovableElement *, GraphicElement *> _elementToGraphicElement;
    std::map<MovableElement *, GraphicElement *> _graphicsToKill;

    std::map<clickableselement,sf::Text> _buttons;

    sf::Texture _background1;
    SlidingBackground *_backgroundSprite1;
    sf::Texture _background2;
    SlidingBackground *_backgroundSprite2;

    sf::Texture Intro;
    GraphicElement * introduction;

    sf::Texture _ball;
    sf::Texture _rock;


    sf::Texture _obstacle;
    sf::Texture _obstacle2;

    sf::Texture _bonus;

    sf::Texture _life;
    GraphicElement* _lifeSprite;
    sf::RectangleShape lifeAmmount;


    sf::Text text;
    sf::Font *_font;
    sf::Font *_font2;

    AnimatedGraphicElement* _button;

    bool _inmenu;
    bool _inintro;

public:
    View(int w, int h);
    ~View();

    void setModel(Model * model);
    void draw();
    void drawMenu();
    bool treatEvents();

    void synchronize();

    bool GetIfMenu() const;
    void setMenu(bool menu);
    void Reset();


};
#endif
