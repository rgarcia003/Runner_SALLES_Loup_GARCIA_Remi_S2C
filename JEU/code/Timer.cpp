#include "Timer.h"

Timer::Timer()
    :m_ElapsedTime{0.0f}, m_State{Started}
{
}
void Timer::Start()
{
    if(m_State != Started) // On ne lance pas le timer si il est déja lancé
    {
        Reset();
        m_State = Started;
    }
}

void Timer::Pause()
{
    if(m_State != Paused) // On ne met pas en pause le timer si il est déja en pause
    {
        m_State = Paused;
        m_ElapsedTime = m_Clock.getElapsedTime().asSeconds();
    }
}

void Timer::Reset()
{

    m_Clock.restart();
}

float Timer::GetTime() const
{
   float time;

    if(m_State == Paused)
    {
        time = m_ElapsedTime;
    }
    else
    {
        time = m_Clock.getElapsedTime().asSeconds() + m_ElapsedTime;
    }

    return time;
}
