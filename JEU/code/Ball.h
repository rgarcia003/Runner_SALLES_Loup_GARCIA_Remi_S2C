#ifndef BALL_H
#define BALL_H

#include"movableelement.h"
#include"bonus.h"
#include"Timer.h"


class Ball:public MovableElement{

private:
    bool _jumping;
    int _life;
    int _lifeMax;
    bool _isVulnerable;
    bool _flying;
    Timer timerFly;
    Timer timerInvincibility;


public:
    Ball(int x, int y, int w, int h, int dx, int dy);
    void move() override;
    void setVulnerable(bool vulnerable);
    bool getVulnerable() const override;
    bool Isjumping() const;
    void setJump (bool jumping);
    bool IsFlying() const;
    void setFly (bool fly);
    int getLife() const override;
    void addLife(int life) override;
    int getLifeMax() const override;
    void setLifeMax(int lifemax);
    void activateBonus(int type);
    void verifyBonus();


};


#endif // BALL_H
