#include "movableelement.h"


MovableElement::MovableElement(float x, float y, float w, float h, float dx, float dy)
    :_x{x},_y(y),_w(w),_h(h),_dx(dx),_dy(dy)
{

}

void MovableElement::move()
{
    _x+=_dx;
    _y+=_dy;
}

//=======================================
// Accesseurs en lecture
//=======================================

int MovableElement::getElemX() const
{
    return _x;
}

int MovableElement::getElemY() const
{
    return _y;
}

sf::Vector2f MovableElement::getElemPosition() const
{
    return sf::Vector2f(_x,_y);
}

sf::Vector2f MovableElement::getElemDimension() const
{
    return sf::Vector2f(_w,_h);
}


int MovableElement::getElemWidth() const
{
    return _w;
}

int MovableElement::getElemHeight() const
{
    return _h;
}


int MovableElement:: getElemSpeedX() const
{
    return _dx;
}

int MovableElement:: getElemSpeedY() const
{
    return _dy;
}

int MovableElement::getType() const// vide car sera redéfini et utilisé par bonus et obstacle uniquement
{
    return -1;
}
int MovableElement::getLife() const
{
    return -1;

}
int MovableElement::getLifeMax() const
{
    return -1;
}
bool MovableElement::getVulnerable() const
{
    return -1;
}


//=======================================
// Accesseurs en écriture
//=======================================
void MovableElement::setElemSpeedX(int dx)
{
    _dx=dx;
}

void MovableElement::setElemSpeedY(int dy)
{
    _dy=dy;
}
void MovableElement::addLife(int life)
{

}
