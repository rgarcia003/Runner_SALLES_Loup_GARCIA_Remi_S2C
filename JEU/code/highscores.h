#ifndef HIGHSCORES_H
#define HIGHSCORES_H
#include "cstdlib"
#include "iostream"
#include "fstream"
#include "View.h"

const std::string SCORES = "../Scores/scores.txt";

class HighScores
{
private:
    sf::Font *_font;
    sf::Text _text;
    std::string _scores;
    std::fstream _fscores;


public:
    HighScores();
    ~HighScores();
    void addScores(unsigned int score);
    void dispScores(sf::RenderWindow *window);
};

#endif // HIGHSCORES_H
