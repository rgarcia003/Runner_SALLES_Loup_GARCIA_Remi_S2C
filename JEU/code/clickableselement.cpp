#include "clickableselement.h"

clickableselement::clickableselement(int x,int y,int w,int h)
    :_x{x}, _y{y}, _w{w}, _h{h}, _isclicked{false}
{}

bool clickableselement::GetClickable(sf::Vector2i pos_Souris)//si le curseur de la souris est sur l'element
{
    if ((pos_Souris.x >= _x && pos_Souris.x <= _x + _w)
            && (pos_Souris.y >= _y && pos_Souris.y <= _y + _h))
    {

        return  true;
    }
    else
    {

        return false;
    }
}

int clickableselement::X()
{
    return _x;
}

int clickableselement::Y()
{
    return _y;
}
int clickableselement::W()
{
    return _w;
}
int clickableselement::H()
{
    return _h;
}
