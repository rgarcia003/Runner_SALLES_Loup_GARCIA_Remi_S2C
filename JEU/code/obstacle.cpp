#include "obstacle.h"

Obstacle:: Obstacle(int x, int y, int w, int h, int dx, int dy, int type)
    :  MovableElement(x,y,w,h,dx,dy), _typeObstacle{type}
{

}

int Obstacle::getType() const
{
    return _typeObstacle;
}
