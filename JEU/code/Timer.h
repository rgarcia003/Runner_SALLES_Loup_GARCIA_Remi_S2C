#ifndef TIMER_H
#define TIMER_H

#include <SFML/System.hpp>

class Timer {

private:
    enum State {Started, Paused};

    sf::Clock m_Clock;
    float m_ElapsedTime;
    State m_State;


public:

    Timer();

    void Start();
    void Pause();
    void Reset();

    float GetTime() const;


};
#endif // TIMER_H
