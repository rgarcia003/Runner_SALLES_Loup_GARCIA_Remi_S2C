/*    Copyright (C) 2016 SALLES Loup and GARCIA Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.*/




#include "View.h"
#include "Model.h"
#include"slidingbackground.h"
#include"globals.h"

int globals::EASYNESS=0;//la variable statique est initialisee

int main(){
    srand(time(NULL));


    Model model(globals::SCREEN_WIDTH, globals::SCREEN_HEIGHT);
    View view(globals::SCREEN_WIDTH, globals::SCREEN_HEIGHT);
    view.setModel(&model);


    while(view.treatEvents()){//tant que view a des evenements a traiter
        while(view.GetIfMenu()){
            view.drawMenu();
            view.setModel(&model);//on fera ici un reset du modele
        }

        model.nextStep();//les elements bougent dans le modele
        view.synchronize();//et leurs graphiques les suivent
        view.draw();

    }

    return EXIT_SUCCESS;
}
