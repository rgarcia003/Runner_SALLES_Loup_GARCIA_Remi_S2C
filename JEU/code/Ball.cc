#include"Ball.h"
#include"globals.h"



Ball:: Ball(int x, int y, int w, int h, int dx, int dy)
    :  MovableElement(x,y,w,h,dx,dy), _jumping{false}, _life{100}, _lifeMax{100},_isVulnerable{true}, _flying{false} {}

void Ball::move()
{

    if(_x+_dx>=0 && _x+_w+_dx<=globals::SCREEN_WIDTH)
        _x+=_dx;
    else
        _dx=0;

    if(_y+_h+_dy<=globals::BALL_HEIGHT+_h && _y+_dy>0)
        _y+=_dy;
    else
        _dy=0;
}

bool Ball::Isjumping() const
{
    return _jumping;
}

void Ball::setJump (bool jumping){
    _jumping=jumping;
}

int Ball::getLife() const
{
    return _life;
}

void Ball::addLife(int life)
{
    _life+=life;
    if(_life<0)
        _life=0;
}

int Ball::getLifeMax() const
{
    return _lifeMax;
}

void Ball::setLifeMax(int lifeMax)
{
    _lifeMax = lifeMax;
}

void Ball::setVulnerable(bool vulnerable)
{
    _isVulnerable = vulnerable;
}

bool Ball::getVulnerable() const
{
    return _isVulnerable;
}

bool Ball::IsFlying() const
{
    return _flying;
}

void Ball::setFly(bool fly)
{
    _flying=fly;
}

void Ball::activateBonus(int type)
{    int l = this->getLife();
     int lm = this->getLifeMax();
      switch(type)
      {

      case 0:
          if (l + 50 >= lm)//bonus de heal
              this->addLife(lm-l);
          else
              this->addLife(50);
          break;

      case 1:// invincibilité
          timerInvincibility.Reset();
          this->setVulnerable(false);


          break;
      case 2://bonus de vol
          timerFly.Reset();
          this->setFly(true);
          break;

      case 3:// armure: la vie max augmente de 20
          this->setLifeMax(lm + 20);
          break;

      }
}

void Ball::verifyBonus()
{
    if(_flying && timerFly.GetTime()>10)//les bonus durent 10 secondes
        _flying=false;
    if(!_isVulnerable && timerInvincibility.GetTime()>10)
    {

       _isVulnerable=true;
    }
}
